CREATE TABLE Theme (
    themeID     integer not null,
    name        varchar(255),
    primary key(themeID)
);

CREATE TABLE Speaker (
    speakerID     integer         not null auto_increment,
    name          varchar(255)    not null,
    themeID       integer         not null,
    primary key(speakerID),
    foreign key (themeID) references Theme(themeID)
);

insert into Theme
    (
        themeID,
        name
    )
values
    (1, 'Educatie'),
    (2, 'Duurzaamheid'),
    (3, 'Vrede & recht'),
    (4, 'Gezonheid & welzijn')
;

insert into Speaker
    (
        name,
        themeID
    )
values
    (
        'Susanne Baars',
        4
    ),
    (
        'Tristan Garssen',
        4
    ),
    (
        'Amber Bindels',
        4
    ),
    (
        'Yannick Kampschoer',
        4
    ),
    (
        'Wouter van Dijk',
        4
    ),
    (
        'Lucille Werner',
        4
    ),
    (
        'Kiki Bosch',
        4
    ),
    (
        'Hans de Jong',
        4
    ),
    (
        'Samira El Idrissi',
        4
    ),
    (
        'Bibian Mentel',
        4
    ),
    (
        'Yousef Yousef',
        2
    ),
    (
        'Anouk van Gijtenbeek',
        2
    ),
    (
        'Stijn Warmenhoven',
        2
    ),
    (
        'Emma Fromberg',
        2
    ),
    (
        'André Kuipers',
        2
    ),
    (
        'MoTown Movement',
        2
    ),
    (
        'Myrthe Frissen',
        2
    ),
    (
        'David Peters',
        2
    ),
    (
        'Bas Timmer',
        2
    ),
    (
        'Edith Schippers',
        2
    ),
    (
        'Joop Roodenburg',
        2
    ),
    (
        'Gyzlene Kramer',
        1
    ),
    (
        'Bashar Alsayegh',
        1
    ),
    (
        'Abdelhamid Idrissi',
        1
    ),
    (
        'Youssef Elbouhassani',
        1
    ),
    (
        'Bas Grund',
        1
    ),
    (
        'Iliass el Hadioui',
        1
    ),
    (
        'Mosa Al Mansour',
        1
    ),
    (
        'Eric van ''t Zelfde',
        1
    ),
    (
        'Rutger de Rijk',
        3
    ),
    (
        'Safiyeh Salehi',
        3
    ),
    (
        'Martijn Kamphorst',
        3
    ),
    (
        'Michael van Praag',
        3
    ),
    (
        'Johnny de Mol en Adil Izemrane',
        3
    ),
    (
        'Zaitoon Shah',
        3
    ),
    (
        'Marit Maij',
        3
    ),
    (
        'Lute Nieuwerth',
        3
    ),
    (
        'Saskia Bruines',
        3
    ),
    (
        'Reinier van Zutphen',
        3
    ),
    (
        'Rob Bauer',
        3
    )
;