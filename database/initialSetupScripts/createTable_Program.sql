CREATE TABLE ProgramBlock (
    programBlockID  integer not null    auto_increment,
    date            date    not null,
    startTime       time    not null,
    endTime         time    not null,
    primary key(programBlockID)
);

CREATE TABLE ProgramBlockItem (
    programBlockItemID  integer         not null    auto_increment,
    programBlockID      integer         not null,
    name                varchar(255)    not null    unique,
    primary key(programBlockItemID),
    foreign key (programBlockID) references ProgramBlock(programBlockID)
);