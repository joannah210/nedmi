CREATE TABLE MenuItem (
    menuItemID  integer         not null auto_increment,
    name        varchar(255)    not null,
    content     varchar(255)    not null,
    placeInMenu integer         not null,
    primary key(menuItemID)
);

insert into MenuItem
(
    name,
    content,
    placeInMenu
)
values
(
    'EssentialInfo',
    'Q&A',
    1
),
(
    'ThemesSubjects',
    'Thema\'s',
    2
),
(
    'ListOfSpeakers',
    'Sprekerslijst',
    3
),
(
    'Schedule',
    'Programma',
    4
),
(
    'MapOfVenue',
    'Plattegrond',
    5
)
;