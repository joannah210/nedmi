CREATE TABLE TextType (
  textTypeID    integer         not null    auto_increment,
  name          varchar(255)    not null    unique ,
  primary key(textTypeID)
);

insert into TextType
(
    name
)
value
('paragraph'),
('header')
;


CREATE TABLE Text (
  textFragmentID    integer         not null    auto_increment,
  textTypeID        integer         not null,
  menuItemID        integer,
  name              varchar(255)    not null    unique,
  content           text            not null,
  primary key(textFragmentID),
  foreign key (textTypeID) references TextType(textTypeID),
  foreign key (menuItemID) references MenuItem(menuItemID)
);



