<?php
include "databaseConnection/DbOperation.php";

// Get input json from post
$post = (array)json_decode(file_get_contents("php://input"));

// Sanitize all input
$themeID = filter_var($post['themeID'], FILTER_SANITIZE_STRING);



if(
    $themeID === '1' ||
    $themeID === '2' ||
    $themeID === '3' ||
    $themeID === '4'

) {
    getSpeaker((int)$themeID);
}

function getSpeaker($themeID)
{
    // Open a connection to the database
    $connection = new DbOperation();

    $query = "select * from Speaker where themeID = $themeID;";
    $result = $connection->select($query);

    $jsonResponse = json_encode($result);
    echo $jsonResponse;
}