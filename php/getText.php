<?php
include "databaseConnection/DbOperation.php";

// Get input json from post
$post = (array)json_decode(file_get_contents("php://input"));

// Sanitize all input
$text = filter_var($post['text'], FILTER_SANITIZE_STRING);

// Open a connection to the database
$connection = new DbOperation();

$query = "select content from Text where name = '$text';";
$result = $connection->select($query);

$jsonResponse = json_encode($result);
echo $jsonResponse;