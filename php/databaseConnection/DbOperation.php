<?php

include "DbConnection.php";

class DbOperation
{

    // CREATE FUNCTIONS
    public function insert($query)
    {
        $connection = new DbConnection();
        mysqli_query($connection->getConnection(), $query);
        $connection->close();
    }

    // READ FUNCTIONS

    // Returns the processed result of any query
    public function select($query)
    {
        $connection = new DbConnection();
        $result = mysqli_query($connection->getConnection(), $query);
        $connection->close();

        $records = array();

        if($result->num_rows > 0) {
            while ($row = $result->fetch_row()) {

                $innerArray = array();

                for ($i = 0; $i < sizeof($row); $i++)
                {
                    array_push($innerArray, $row[$i]);
                }

                array_push($records, $innerArray);
            }
        }

        return $records;
    }

    // Selects a column of a certain table
    // and returns this column as an one-dimensional array
    public function selectColumn($column, $table)
    {
        $connection = new DbConnection();
        $query = "select $column from $table;";
        $result = mysqli_query($connection->getConnection(), $query);
        $connection->close();

        $records = array();

        if($result->num_rows > 0) {
            while ($row = $result->fetch_assoc())
            {
                array_push($records, $row[$column]);
            }
        }
        return $records;
    }

    // Selects columns of a certain table
    // and returns this column as an two-dimensional array
    public function selectNColumns($table, ...$columns)
    {
        $connection = new DbConnection();

        $c = "";
        foreach ($columns as $column)
        {
            $c = $c.$column.", ";
        }
        $c = substr($c, 0, -2);

        $query = "select ".$c." from $table;";
        $result = mysqli_query($connection->getConnection(), $query);
        $connection->close();

        $records = array();

        if($result->num_rows > 0) {
            while ($row = $result->fetch_row()) {

                $innerArray = array();

                for ($i = 0; $i < sizeof($row); $i++)
                {
                    array_push($innerArray, $row[$i]);
                }

                array_push($records, $innerArray);
            }
        }

        return $records;
    }

    // UPDATE FUNCTIONS


    // DELETE FUNCTIONS
}