<?php

class DbConnection
{
    private $connection;

    function __construct()
    {
        $config = parse_ini_file('/var/www/noDirectAccess/config.ini');
        $server = $config['server'];
        $username = $config['username'];
        $password = $config['password'];
        $database = $config['database'];

        $this->connection = new mysqli($server, $username, $password, $database);

        if (!$this->connection) {
            die("Connection failed: " . mysqli_connect_error());
        } else {
            //echo "Connected successfully to database";
            //echo "\n";
        }
    }

    public function close()
    {
        mysqli_close($this->connection);
        //echo "Database connection closed";
        //echo "\n";
    }

    public function getConnection()
    {
        return $this->connection;
    }

}







