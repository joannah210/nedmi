import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from "@material-ui/styles";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import './stylesheets/App.css';

import ButtonAppBar from "./components/ButtonAppBar";
import Footer from "./components/Footer";
import {Menu} from "./components/Menu";
import MainContent from "./components/MainContent";


const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#F16A24'
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        }
    },

});

export default class App extends React.Component{

    constructor(props)
    {
        super(props);

        this.state = {
            isMenuVisible: false,
            isMainContentVisible: true,
            titleAppBar: 'Welkom',
            page: ''
        };

        this.changeMenuVisibility = this.changeMenuVisibility.bind(this);
        this.changeTitleAppBar = this.changeTitleAppBar.bind(this);
        this.changePage = this.changePage.bind(this);
    }

    changeMenuVisibility()
    {
        this.setState({
            isMenuVisible: !this.state.isMenuVisible,
            // MainContent visibility is always the reverse Menu visibility
            isMainContentVisible: !this.state.isMainContentVisible
        });
    }

    changeTitleAppBar(title)
    {
        this.setState({
            titleAppBar: title
        })
    }

    changePage(page)
    {
        this.setState({
            page: page
        })
    }

    render() {
        return(
            <React.Fragment>
                <CssBaseline />
                {/* The rest of your application */
                    <ThemeProvider theme={theme}>

                        <ButtonAppBar   id='header'
                                        title={this.state.titleAppBar}
                                        changeMenuVisible={this.changeMenuVisibility}
                        />

                        <MainContent    id='mainContent'
                                        visible={this.state.isMainContentVisible}
                                        page={this.state.page}
                        />

                        <Menu   id='menu'
                                visible={this.state.isMenuVisible}
                                changeTitle={this.changeTitleAppBar}
                                changePage={this.changePage}
                                changeMenuVisible={this.changeMenuVisibility}

                        />

                        <Footer id='footer'
                                menuVisible={this.state.isMenuVisible}
                        />
                    </ThemeProvider>
                }
            </React.Fragment>
        );
    }

}


