import React from 'react';
import logo from '../images/logo.svg';
import '../stylesheets/Footer.css';

export default function Footer(props)
{

    let color;

    if(props.menuVisible === true)
    {
        color = '#F0F0F0';
    }else{
        color = '#FAFAFA';
    }

    let style = {
        backgroundColor: color
    };

    return (
        <div id='footerHolder' style={style}>
            <div id='leftStripesHolder'>
            </div>
            <div id='logoHolder'>
                <img src={logo} alt="NedMILogo" id='nedmiLogo'/>
            </div>
            <div id='rightStripesHolder'>
            </div>
        </div>
    );
}