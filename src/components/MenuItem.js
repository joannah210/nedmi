import React from 'react';
import Typography from "@material-ui/core/Typography";


export class MenuItem extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    setTitle(title)
    {
        this.props.changeTitle(title);
        // After the title is set we also set the page
        this.props.changePage(this.props.page);
        // After the title is set we hide the menu
        this.props.changeMenuVisible();
    }

    render() {
        return (
            <div className='menuItem' onClick={this.setTitle.bind(this, this.props.content)}>
                <Typography variant='body2'>
                    {this.props.content}
                </Typography>
            </div>
        );
    }
}

