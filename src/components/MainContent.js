import React from 'react';
import '../stylesheets/MainContent.css';
import Info from "./mainContent/Info";
import Themes from "./mainContent/Themes";
import Speakers from "./mainContent/Speakers";
import Program from "./mainContent/Program";
import VenueMap from "./mainContent/VenueMap";
import Welcome from "./mainContent/Welcome";

export default function MainContent(props)
{
    const content =
    (
        <div id='mainContentHolder'>
            {props.page === ''                  && <Welcome />}
            {props.page === 'EssentialInfo'     && <Info />}
            {props.page === 'ThemesSubjects'    && <Themes />}
            {props.page === 'ListOfSpeakers'    && <Speakers />}
            {props.page === 'Schedule'          && <Program />}
            {props.page === 'MapOfVenue'        && <VenueMap />}
        </div>
    );

    return (
        props.visible === true && content
    );
}