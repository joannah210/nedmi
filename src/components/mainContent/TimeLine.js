import React, {createElement} from 'react';
import '../../stylesheets/Program.css';

export class TimeLine extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state =
        {
            hourOfDay: new Date().getHours(),
            minutesOfDay: new Date().getMinutes()
        }
    }

    componentDidMount() {
        this.interval = setInterval(() => this.setState({
            hourOfDay: new Date().getHours(),
            minutesOfDay: new Date().getMinutes(),
        }), 1000);
    }

    componentWillUnmount()
    {
        clearInterval(this.interval);
    }

    render()
    {

        let startOfDay = 0;
        let endOfDay = 0;

        let date = new Date().getDate();

        if(date === 23)
        {
            startOfDay = 17;
            endOfDay = 22;
        }


        const hoursToDisplay = endOfDay - startOfDay;
        // One timeUnit is one minute
        const timeUnitsToDisplay = (hoursToDisplay * 60);

        let past = 0;
        if(this.state.hourOfDay >= startOfDay)
        {
            past = ((this.state.hourOfDay - startOfDay) * 60) + (this.state.minutesOfDay);
        }

        const future = (timeUnitsToDisplay - past);

        const timeUnitPast = <div className='timeUnit' id='past'/>;
        const timeUnitFuture = <div className='timeUnit' id='future'/>;

        let content = [];

        for (let i = 0; i < past; i++)
        {
            content.push(timeUnitPast);
        }

        for (let i = 0; i < future; i++)
        {
            content.push(timeUnitFuture);
        }

        return(
            <div id='timeLine'>
                {content}
            </div>
        );
    }
}