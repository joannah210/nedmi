import React from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import Typography from "@material-ui/core/Typography";


export default class ThemeHeader extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            expand: false
        };

    }

    changeExpand()
    {
        this.setState({
            expand: !this.state.expand
        })
    }

    render() {
        return (
            <div className='themeHolder'>
                <img className='themeLogo' src={this.props.themeLogo} alt='themeLogo' onClick={this.changeExpand.bind(this)}/>
                <Typography variant="h5" className='theme' onClick={this.changeExpand.bind(this)}>{this.props.title}</Typography>
                {this.state.expand === true && <ExpandLessIcon onClick={this.changeExpand.bind(this)}/>}
                {this.state.expand === false && <ExpandMoreIcon onClick={this.changeExpand.bind(this)}/>}
                {this.state.expand === true && <Typography variant="body1" className='info'>{this.props.content}</Typography>}
            </div>
        );
    }


}