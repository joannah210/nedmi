import React from 'react';
import mapImage from '../../images/fokkerTerminalMap.png';
import PinchZoomPan from "react-responsive-pinch-zoom-pan";

export default function VenueMap()
{
    return (
        <div style={{ width: '90%', height: '400px', marginLeft: 'auto', marginRight: 'auto' }}>
            <PinchZoomPan>
                <img alt='Test Image' src={mapImage}/>
            </PinchZoomPan>
        </div>
    );
}