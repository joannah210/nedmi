import React from 'react';
import '../../stylesheets/Themes.css';
import eduLogo from '../../images/Education_Orange.svg';
import peaceLogo from '../../images/Peace_Orange.svg';
import healthLogo from '../../images/Health_Orange.svg';
import susLogo from '../../images/Sustainability_Orange.svg';
import ThemeHeader from "./ThemeHeader";

export default class Themes extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            infoEducation: '',
            infoSustainability: '',
            infoPeace: '',
            infoHealth: ''
        };
    }

    async getThemeDescription(text)
    {
        const body =
        {
            text: text
        };

        const res = await fetch('/php/getText.php', {
            method: 'post',
            body: JSON.stringify(body),
            headers: { 'Content-type': 'application/json' }
        });

        const data = await res.json();

        switch(text) {
            case 'theme.education.description':
                this.setState({
                    infoEducation: data
                });
                break;
            case 'theme.sustainability.description':
                this.setState({
                    infoSustainability: data
                });
                break;
            case 'theme.peace.description':
                this.setState({
                    infoPeace: data
                });
                break;
            case 'theme.health.description':
                this.setState({
                    infoHealth: data
                });
                break;
        }
    }

    componentDidMount()
    {
        this.getThemeDescription('theme.education.description');
        this.getThemeDescription('theme.sustainability.description');
        this.getThemeDescription('theme.peace.description');
        this.getThemeDescription('theme.health.description');
    }

    render()
    {
        return(
            <div>
                <ThemeHeader title='Educatie' content={this.state.infoEducation} themeLogo={eduLogo}/>
                <ThemeHeader title='Duurzaamheid' content={this.state.infoSustainability} themeLogo={susLogo}/>
                <ThemeHeader title='Vrede & Recht' content={this.state.infoPeace} themeLogo={peaceLogo}/>
                <ThemeHeader title='Gezondheid & welzijn' content={this.state.infoHealth} themeLogo={healthLogo}/>
            </div>
        );
    }
}