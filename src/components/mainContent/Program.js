import React from 'react';
import '../../stylesheets/Program.css';
import {TimeLine} from "./TimeLine";
import {Times} from "./Times";

export default function Program()
{
    return (
        <div id='programHolder'>
            <Times />
            <TimeLine/>
        </div>
    );
}