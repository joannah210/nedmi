import React from "react";
import Typography from "@material-ui/core/Typography";

export default class Welcome extends React.Component
{
    constructor(props) {
        super(props);

        this.state = {
            welcomeText: ''
        }

    }

    componentDidMount() {
        this.getText();
    }

    async getText()
    {
        const body =
            {
                text: 'welcome'
            };

        const res = await fetch('/php/getText.php', {
            method: 'post',
            body: JSON.stringify(body),
            headers: { 'Content-type': 'application/json' }
        });

        const data = await res.json();

        this.setState({
            welcomeText: data
        })
    }

    render()
    {
        return (
            <Typography variant="body1" id='welcomeText'>{this.state.welcomeText}</Typography>
        );
    }

}