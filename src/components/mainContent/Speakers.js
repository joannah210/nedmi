import React from 'react';
import SpeakerHeader from "./SpeakerHeader";
import '../../stylesheets/Speakers.css';
import eduLogo from '../../images/Education_Orange.svg';
import peaceLogo from '../../images/Peace_Orange.svg';
import healthLogo from '../../images/Health_Orange.svg';
import susLogo from '../../images/Sustainability_Orange.svg';

export default class Speakers extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            speakersEducation: [],
            speakersSustainability: [],
            speakersPeace: [],
            speakersHealth: []
        };
    }

    async getSpeakers(themeID)
    {
        const body =
        {
            themeID: themeID
        };

        const res = await fetch('/php/getSpeakers.php', {
            method: 'post',
            body: JSON.stringify(body),
            headers: { 'Content-type': 'application/json' }
        });

        const data = await res.json();

        switch(themeID) {
            case 1:
                this.setState({
                    speakersEducation: data
                });
                break;
            case 2:
                this.setState({
                    speakersSustainability: data
                });
                break;
            case 3:
                this.setState({
                    speakersPeace: data
                });
                break;
            case 4:
                this.setState({
                    speakersHealth: data
                });
                break;
        }
    }

    componentDidMount()
    {
        this.getSpeakers(1);
        this.getSpeakers(2);
        this.getSpeakers(3);
        this.getSpeakers(4);
    }

    render()
    {
        const listEducation = Object.entries(this.state.speakersEducation).map((entry) =>
            <div>{entry[1][1]}</div>
        );

        const listSustainability = Object.entries(this.state.speakersSustainability).map((entry) =>
            <div>{entry[1][1]}</div>
        );

        const listPeace = Object.entries(this.state.speakersPeace).map((entry) =>
            <div>{entry[1][1]}</div>
        );

        const listHealth = Object.entries(this.state.speakersHealth).map((entry) =>
            <div>{entry[1][1]}</div>
        );

        return(
            <div>
                <SpeakerHeader title='Educatie' list={listEducation} themeLogo={eduLogo}/>
                <SpeakerHeader title='Duurzaamheid' list={listSustainability} themeLogo={susLogo}/>
                <SpeakerHeader title='Vrede & Recht' list={listPeace} themeLogo={peaceLogo}/>
                <SpeakerHeader title='Gezondheid & welzijn' list={listHealth} themeLogo={healthLogo}/>
            </div>
        );
    }
}