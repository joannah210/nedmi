import React from 'react';
import '../stylesheets/Menu.css';
import top from '../images/top.svg';
import MenuItemHolder from "./MenuItemHolder";

export function Menu(props)
{
    const content = (
        <div id='menuHolder'>
            <img id='menuTopHolder' src={top} alt='top'/>
            <MenuItemHolder changeTitle={props.changeTitle}
                            changePage={props.changePage}
                            changeMenuVisible={props.changeMenuVisible}
            />
        </div>
    );

    return(
        props.visible === true && content
    )
}