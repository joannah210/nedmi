import React from 'react';
import {MenuItem} from './MenuItem';

export default class MenuItemHolder extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            menuItems: [],
        };
    }

    async getMenuItems()
    {
        const body =
            {
                getMenuItems: 'true'
            };

        const res = await fetch('/php/getMenuItems.php', {
            method: 'post',
            body: JSON.stringify(body),
            headers: { 'Content-type': 'application/json' }
        });

        const data = await res.json();

        this.setState({
            menuItems: data
        })
    }

    componentDidMount() {
        this.getMenuItems();
    }



    render() {

        const arrayOfMenuItems = this.state.menuItems;

        const list = Object.entries(arrayOfMenuItems).map((entry) =>
            <MenuItem content={entry[1][1]}
                      page={entry[1][0]}
                      nameMenuItem={entry[1][0]}
                      changeTitle={this.props.changeTitle}
                      changePage={this.props.changePage}
                      changeMenuVisible={this.props.changeMenuVisible}
            />
        );

        return (
            <div id='menuItemHolder'>
                {list}
            </div>
        );
    }
}

