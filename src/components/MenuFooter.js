import React from 'react';
import logo from '../images/logo.svg';

export default function MenuFooter()
{
    return (
        <div id='menuFooter'>
            <div id='leftStripesHolder'>
            </div>
            <div id='logoHolder'>
                <img src={logo} alt="NedMILogo" />
            </div>
            <div id='rightStripesHolder'>
            </div>
        </div>
    );
}